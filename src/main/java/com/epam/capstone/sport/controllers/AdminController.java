package com.epam.capstone.sport.controllers;

import com.epam.capstone.sport.models.*;
import com.epam.capstone.sport.service.OrderServiceImpl;
import com.epam.capstone.sport.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private ProductServiceImpl productService;

    @Autowired
    private OrderServiceImpl orderService;

    @GetMapping("/createProduct")
    @PreAuthorize("hasRole('ADMIN')")
    public String productCreateGet(Model model) {
        model.addAttribute("categories", productService.getAllCategories());
        model.addAttribute("Product", new Product());

        return "productCreate";
    }

    @PostMapping("/createProduct")
    @PreAuthorize("hasRole('ADMIN')")
    public String productCreatePost(@ModelAttribute Product product, @RequestParam("categoryId") Long categoryId) {
        productService.createProduct(product, categoryId);

        return "redirect:/products";
    }

    @GetMapping("/editProduct/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String editProduct(Model model, @PathVariable Long id) {
        model.addAttribute("product", productService.getProductById(id));
        model.addAttribute("categories", productService.getAllCategoriesExceptCurrent(id));

        return "productEdit";
    }

    @PostMapping("/editProduct/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String editProductPost(@RequestParam("categoryId") Long categoryId, @ModelAttribute Product product, @PathVariable Long id) {
        productService.updateProduct(id, product, categoryId);

        return "redirect:/products";
    }


    @GetMapping("/orders")
    @PreAuthorize("hasRole('ADMIN')")
    public String orders(Model model) {
        model.addAttribute("orders", orderService.getAllOrders());

        return "orders";
    }

    @GetMapping("/orders/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String order(Model model, @PathVariable Long id) {
        model.addAttribute("order", orderService.getOrderById(id));
        model.addAttribute("products", orderService.getAllProductEntriesById(id));
        model.addAttribute("statuses", orderService.getAllStatusesExceptCurrent(id));

        return "order";
    }

    @PostMapping("/orders/updateStatus/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String orderUpdate(@PathVariable Long id, @RequestParam(name = "statusId") Long statusId) {
        orderService.updateOrderStatus(id, statusId);

        return "redirect:/admin/orders/" + id;
    }
}
