package com.epam.capstone.sport.controllers;

import com.epam.capstone.sport.service.CartServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartServiceImpl cartService;

    @GetMapping("")
    @PreAuthorize("hasRole('USER')")
    public String cart(Model model, Principal principal) {
        model.addAttribute("products", cartService.getProductEntries(principal));
        model.addAttribute("totalPrice", cartService.calculateTotalPrice(principal));

        return "cart";
    }

    @PostMapping("/addCart/{id}")
    @PreAuthorize("hasRole('USER')")
    public String addToCart(Principal principal, @PathVariable(value = "id") Long id,
                            @RequestParam("quantity") int quantity) {
        cartService.addToCart(principal, id, quantity);

        return "redirect:/products";
    }

    @PostMapping("/removeCart/{id}")
    @PreAuthorize("hasRole('USER')")
    public String removeFromCart(@PathVariable Long id) {
        cartService.removeFromCart(id);

        return "redirect:/cart";
    }
}
