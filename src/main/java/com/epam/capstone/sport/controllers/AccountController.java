package com.epam.capstone.sport.controllers;

import com.epam.capstone.sport.models.CardInfo;
import com.epam.capstone.sport.service.CartServiceImpl;
import com.epam.capstone.sport.service.OrderServiceImpl;
import com.epam.capstone.sport.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/account")
public class AccountController {

    private final UserServiceImpl userService;
    private final OrderServiceImpl orderService;
    private final CartServiceImpl cartService;

    @Autowired
    public AccountController(UserServiceImpl UserServiceImpl, OrderServiceImpl orderService, CartServiceImpl cartService) {
        this.userService = UserServiceImpl;
        this.orderService = orderService;
        this.cartService = cartService;
    }

    @GetMapping("")
    @PreAuthorize("hasRole('USER')")
    public String info(Model model, Principal principal) {
        model.addAttribute("user", userService.findByEmail(principal.getName()));

        return "account";
    }

    @PostMapping("/addInfo")
    @PreAuthorize("hasRole('USER')")
    public String addInfo(Principal principal, @ModelAttribute CardInfo card) {
        userService.saveCardInfo(principal.getName(), card);

        return "redirect:/account";
    }

    @PostMapping("/changePassword")
    @PreAuthorize("hasRole('USER')")
    public String changePassword(Principal principal, @RequestParam String password) {
        userService.changePassword(principal.getName(), password);

        return "redirect:/account";
    }

    @PostMapping("/changeAddress")
    @PreAuthorize("hasRole('USER')")
    public String changeAddress(Principal principal, @RequestParam String address) {
        userService.changeAddress(principal.getName(), address);

        return "redirect:/account";
    }

    @PostMapping("/deleteInfo")
    @PreAuthorize("hasRole('USER')")
    public String deleteInfo(Principal principal) {
        userService.deleteInfo(principal.getName());

        return "redirect:/account";
    }

    @PostMapping("/delete")
    @PreAuthorize("hasRole('USER')")
    public String deleteAccount(Principal principal) {
        orderService.deleteAllByUser(userService.findByEmail(principal.getName()));
        cartService.deleteCart(userService.findByEmail(principal.getName()));
        userService.deleteAccount(principal.getName());

        return "redirect:/logout";
    }
}
