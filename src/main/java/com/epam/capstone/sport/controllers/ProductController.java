package com.epam.capstone.sport.controllers;

import com.epam.capstone.sport.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class ProductController {
    @Autowired
    private ProductServiceImpl productService;

    @GetMapping("/")
    public String home() {
        return "index";
    }

    @GetMapping("/products")
    public String products(Model model) {
        model.addAttribute("products", productService.getAllProducts());

        return "products";
    }
    @GetMapping("/products/{id}")
    public String productCard(Model model, @PathVariable(value = "id") Long id) {
        model.addAttribute("product", productService.getProductById(id));

        return "product";
    }


}
