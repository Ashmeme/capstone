package com.epam.capstone.sport.controllers;

import com.epam.capstone.sport.service.UserServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AuthController {

    @Autowired
    private UserServiceImpl authService;

    @GetMapping("/login")
    public String login() {
        return "signin";
    }
    @GetMapping("/register")
    public String register() {
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(Model model, @RequestParam String email, @RequestParam String password ) {
        try{
            authService.registerUser(email, password);

            return "redirect:/login";

        } catch (Exception e){
            model.addAttribute("error", e);

            return "register";
        }
    }

}
