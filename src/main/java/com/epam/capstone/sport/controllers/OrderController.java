package com.epam.capstone.sport.controllers;

import com.epam.capstone.sport.service.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
@Controller
@RequestMapping("/orders")
public class OrderController {
    @Autowired
    private OrderServiceImpl orderService;

    @GetMapping("")
    @PreAuthorize("hasRole('USER')")
    public String orders(Model model, Principal principal) {
        model.addAttribute("orders", orderService.getAllOrdersByUser(principal));

        return "orders";
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER')")
    public String order(Model model, @PathVariable Long id) {
        model.addAttribute("order", orderService.getOrderById(id));
        model.addAttribute("products", orderService.getAllProductEntriesById(id));

        return "order";
    }

    @PostMapping("/createOrder")
    @PreAuthorize("hasRole('USER')")
    public String orders(Principal principal) {
        return orderService.createOrder(principal.getName());
    }
}
