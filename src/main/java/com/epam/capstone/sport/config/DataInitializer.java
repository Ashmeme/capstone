package com.epam.capstone.sport.config;

        import com.epam.capstone.sport.models.AuthGroup;
        import com.epam.capstone.sport.models.Category;
        import com.epam.capstone.sport.models.OrderStatus;
        import com.epam.capstone.sport.repository.AuthGroupRepository;
        import com.epam.capstone.sport.repository.CategoryRepository;
        import com.epam.capstone.sport.repository.OrderStatusRepository;
        import com.epam.capstone.sport.repository.UserRepository;
        import com.epam.capstone.sport.service.UserServiceImpl;
        import com.epam.capstone.sport.service.ProductServiceImpl;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Component;

        import jakarta.annotation.PostConstruct;

@Component
public class DataInitializer {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthGroupRepository authGroupRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private OrderStatusRepository orderStatusRepository;
    @Autowired
    private final UserServiceImpl authService;
    @Autowired
    private final ProductServiceImpl productService;


    public DataInitializer(UserServiceImpl authService, ProductServiceImpl productService) {
        this.authService = authService;
        this.productService = productService;
    }

    @PostConstruct
    public void initData() {
        if (categoryRepository.count() == 0) {
            categoryRepository.save(new Category("Clothes"));
            categoryRepository.save(new Category("Weights"));
            categoryRepository.save(new Category("Food"));
            categoryRepository.save(new Category("Training Inventory"));
        }

        if (orderStatusRepository.count() == 0) {
            orderStatusRepository.save(new OrderStatus("Processing Order"));
            orderStatusRepository.save(new OrderStatus("Order Shipped"));
            orderStatusRepository.save(new OrderStatus("In Delivery"));
            orderStatusRepository.save(new OrderStatus("Delivered"));

        }

        if (userRepository.count() == 0) {
            authService.registerUser("user@gmail.com", "123");
//            productService.createProduct("Weights, 5kg","Hand held weights, made out of lead.", "Costco", "Weights",10.0,100);
//            productService.createProduct("Weights, 10kg","Hand held weights, made out of lead.", "Costco", "Weights",12.5,100);
//            productService.createProduct("Weights, 15kg","Hand held weights, made out of lead.", "Costco", "Weights",15.0,100);
//            productService.createProduct("Weights, 20kg","Hand held weights, made out of lead.", "Costco", "Weights",17.5,100);

//            productService.createProduct("Protein bar","Nutritous snack.", "Wallmart", "Food",5.0,100);
//            productService.createProduct("Protein powder","Nutritous powder.", "Wallmart", "Food",50.0,100);
//            productService.createProduct("Tshirt","A white T", "Target", "Clothes",20.0,100);

            authService.registerUser("adilbek@gmail.com", "123");
            AuthGroup authGroup = authGroupRepository.findOneByEmail("adilbek@gmail.com");
            authGroup.setAuthGroup("ADMIN");
            authGroupRepository.save(authGroup);

        }
    }
}
