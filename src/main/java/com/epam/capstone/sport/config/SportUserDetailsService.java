package com.epam.capstone.sport.config;

import com.epam.capstone.sport.models.AuthGroup;
import com.epam.capstone.sport.models.User;
import com.epam.capstone.sport.repository.AuthGroupRepository;
import com.epam.capstone.sport.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SportUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;
    private final AuthGroupRepository authGroupRepository;

    public SportUserDetailsService(UserRepository userRepository, AuthGroupRepository authGroupRepository){
        super();
        this.userRepository = userRepository;
        this.authGroupRepository = authGroupRepository;
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.findByEmail(username);
        if (user == null){
            throw new UsernameNotFoundException("cannot find User" + username);
        }
        List<AuthGroup> authGroups = this.authGroupRepository.findByEmail(username);
        return new UserPrincipal(user, authGroups);
    }



}
