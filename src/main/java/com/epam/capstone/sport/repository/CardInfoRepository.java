package com.epam.capstone.sport.repository;

import com.epam.capstone.sport.models.CardInfo;
import com.epam.capstone.sport.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardInfoRepository extends JpaRepository<CardInfo,Long> {


}
