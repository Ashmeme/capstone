package com.epam.capstone.sport.repository;

import com.epam.capstone.sport.models.Category;
import com.epam.capstone.sport.models.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderStatusRepository extends JpaRepository<OrderStatus, Long> {
     OrderStatus findByName(String status);
}
