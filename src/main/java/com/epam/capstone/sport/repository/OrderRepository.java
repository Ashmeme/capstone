package com.epam.capstone.sport.repository;

import com.epam.capstone.sport.models.OrderDetails;
import com.epam.capstone.sport.models.User;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<OrderDetails, Long> {
    List<OrderDetails> findAllByUser(User user);

    @Transactional
    void deleteAllByUser(User user);
}
