package com.epam.capstone.sport.repository;

import com.epam.capstone.sport.models.ShoppingCart;
import com.epam.capstone.sport.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart,Long> {
    ShoppingCart findByUser(User user);
}
