package com.epam.capstone.sport.repository;

import com.epam.capstone.sport.models.Category;
import com.epam.capstone.sport.models.Product;
import com.epam.capstone.sport.models.ProductEntry;
import com.epam.capstone.sport.models.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductEntryRepository extends JpaRepository<ProductEntry,Long> {
    List<ProductEntry> findAllByCart(ShoppingCart cart);

    ProductEntry findByCartAndProduct(ShoppingCart cart, Product product);
}
