package com.epam.capstone.sport.repository;

import com.epam.capstone.sport.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Long> {


    Category findByName(String category);
}
