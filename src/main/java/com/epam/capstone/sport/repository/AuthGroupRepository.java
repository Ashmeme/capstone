package com.epam.capstone.sport.repository;

import com.epam.capstone.sport.models.AuthGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuthGroupRepository extends JpaRepository<AuthGroup, Long> {
    List<AuthGroup> findByEmail(String email);

    AuthGroup findOneByEmail(String email);
}
