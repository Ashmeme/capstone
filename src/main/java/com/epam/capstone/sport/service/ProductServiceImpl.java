package com.epam.capstone.sport.service;


import com.epam.capstone.sport.models.*;
import com.epam.capstone.sport.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProducts(){
        return productRepository.findAll();
    }

    public Product getProductById(Long id){
        return productRepository.findById(id).orElseThrow();
    }
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    public Product createProduct(Product product, Long categoryId) {
        Category category = categoryRepository.findById(categoryId).orElse(null);
        product.setCategory(category);

        return productRepository.save(product);
    }
    public Product createProduct(String name, String description, String brand, String category_name, double price, int stock) {
        Product product = new Product();
        product.setName(name);
        product.setDescription(description);
        product.setBrand(brand);
        Category category = categoryRepository.findByName(category_name);
        product.setCategory(category);
        product.setPrice(price);
        product.setStock(stock);

        return productRepository.save(product);
    }
    public List<Category> getAllCategoriesExceptCurrent(Long id) {
        Product product = getProductById(id);
        List<Category> categories = categoryRepository.findAll();
        categories.remove(product.getCategory());
        return categories;
    }

    public Product updateProduct(Long id, Product product, Long categoryId) {
        product.setId(id);
        Category category = categoryRepository.findById(categoryId).orElse(null);
        product.setCategory(category);

        return productRepository.save(product);
    }


}
