package com.epam.capstone.sport.service;

import com.epam.capstone.sport.models.*;

import java.security.Principal;
import java.util.List;

public interface CartService {
    ShoppingCart getCartByUser(Principal principal);

    List<ProductEntry> getProductEntries(Principal principal);

    double calculateTotalPrice(Principal principal);

    void addToCart(Principal principal, Long productId, int quantity);

    void removeFromCart(Long id);

}
