package com.epam.capstone.sport.service;

import com.epam.capstone.sport.models.AuthGroup;
import com.epam.capstone.sport.models.ShoppingCart;
import com.epam.capstone.sport.models.User;
import com.epam.capstone.sport.models.CardInfo;
import com.epam.capstone.sport.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;



@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final CardInfoRepository cardInfoRepository;
    private final ShoppingCartRepository shoppingCartRepository;
    private final AuthGroupRepository authGroupRepository;
    private final PasswordEncoder encryptor;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, CardInfoRepository cardInfoRepository,
                           ShoppingCartRepository shoppingCartRepository, AuthGroupRepository authGroupRepository,
                           PasswordEncoder encryptor) {
        this.userRepository = userRepository;
        this.cardInfoRepository = cardInfoRepository;
        this.shoppingCartRepository = shoppingCartRepository;
        this.authGroupRepository = authGroupRepository;
        this.encryptor = encryptor;
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public void registerUser(String email, String password) {
        User user = new User(email, encryptor.encode(password));
        userRepository.save(user);
        ShoppingCart cart = new ShoppingCart();
        cart.setUser(user);
        shoppingCartRepository.save(cart);
        authGroupRepository.save(new AuthGroup(email, "USER"));
    }

    @Override
    public void saveCardInfo(String email, CardInfo card) {
        User user = userRepository.findByEmail(email);
        card = encryptCardInfo(card);
        cardInfoRepository.save(card);
        user.setCard(card);
        userRepository.save(user);
    }

    public CardInfo encryptCardInfo( CardInfo card) {

        card.setCardNumber(encryptor.encode(card.getCardNumber()));
        card.setCvv2(encryptor.encode(card.getCvv2()));
        card.setExpDate(encryptor.encode(card.getExpDate()));
        card.setCardHolderName(encryptor.encode(card.getCardHolderName()));

        return card;
    }

    @Override
    public void changePassword(String email, String password) {
        User user = userRepository.findByEmail(email);
        user.setPassword(encryptor.encode(password));
        userRepository.save(user);
    }

    @Override
    public void changeAddress(String email, String address) {
        User user = userRepository.findByEmail(email);
        user.setAddress(address);
        userRepository.save(user);
    }

    @Override
    public void deleteInfo(String email) {
        User user = userRepository.findByEmail(email);
        user.setCard(null);
        user.setAddress(null);
        userRepository.save(user);
    }

    @Override
    public void deleteAccount(String email) {
        User user = userRepository.findByEmail(email);
        userRepository.delete(user);
    }
}
