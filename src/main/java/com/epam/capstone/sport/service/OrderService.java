package com.epam.capstone.sport.service;

import com.epam.capstone.sport.models.*;

import java.security.Principal;
import java.util.List;

public interface OrderService {

    String createOrder(String email);

    List<OrderDetails> getAllOrders();

    List<OrderDetails> getAllOrdersByUser(Principal principal);

    OrderDetails getOrderById(Long id);

    List<OrderStatus> getAllStatusesExceptCurrent(Long id);

    List<ProductEntry> getAllProductEntriesById(Long id);

    OrderDetails updateOrderStatus(Long id, Long statusId);

    void deleteAllByUser(User user);

}
