package com.epam.capstone.sport.service;

import com.epam.capstone.sport.models.Category;
import com.epam.capstone.sport.models.Product;

import java.util.List;

public interface ProductService {
    public List<Product> getAllProducts();

    Product getProductById(Long id);

    List<Category> getAllCategories();

    Product createProduct(Product product, Long categoryId);

    Product createProduct(String name, String description, String brand,
                          String category_name, double price, int stock);

    List<Category> getAllCategoriesExceptCurrent(Long id);

    Product updateProduct(Long id, Product product, Long categoryId);

}
