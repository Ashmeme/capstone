package com.epam.capstone.sport.service;

import com.epam.capstone.sport.models.Product;
import com.epam.capstone.sport.models.ProductEntry;
import com.epam.capstone.sport.models.ShoppingCart;
import com.epam.capstone.sport.models.User;
import com.epam.capstone.sport.repository.ProductEntryRepository;
import com.epam.capstone.sport.repository.ProductRepository;
import com.epam.capstone.sport.repository.ShoppingCartRepository;
import com.epam.capstone.sport.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private ProductEntryRepository productEntryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    public ShoppingCart getCartByUser(Principal principal) {
        User user = userRepository.findByEmail(principal.getName());

        return shoppingCartRepository.findByUser(user);
    }

    public List<ProductEntry> getProductEntries(Principal principal) {
        ShoppingCart cart = getCartByUser(principal);
        return productEntryRepository.findAllByCart(cart);
    }

    public double calculateTotalPrice(Principal principal) {
        List<ProductEntry> products = getProductEntries(principal);
        return products.stream().mapToDouble(product -> product.getQuantity() * product.getProduct().getPrice()).sum();
    }

    public void addToCart(Principal principal, Long productId, int quantity) {
        User user = userRepository.findByEmail(principal.getName());
        Product product = productRepository.findById(productId).orElseThrow();
        ShoppingCart cart = shoppingCartRepository.findByUser(user);
        ProductEntry existingEntry = productEntryRepository.findByCartAndProduct(cart, product);

        if (existingEntry != null) {
            existingEntry.setQuantity(existingEntry.getQuantity() + quantity);
            productEntryRepository.save(existingEntry);
        } else {
            ProductEntry productEntry = new ProductEntry();
            productEntry.setCart(cart);
            productEntry.setProduct(product);
            productEntry.setQuantity(quantity);
            productEntryRepository.save(productEntry);
        }
    }

    public void removeFromCart(Long id) {
        productEntryRepository.deleteById(id);
    }

    public void deleteCart(User user){
        ShoppingCart cart = shoppingCartRepository.findByUser(user);
        shoppingCartRepository.delete(cart);
    }
}
