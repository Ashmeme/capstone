package com.epam.capstone.sport.service;

import com.epam.capstone.sport.models.CardInfo;
import com.epam.capstone.sport.models.User;

public interface UserService {
     User findByEmail(String email) ;

     void registerUser(String email, String password);

     void saveCardInfo(String email, CardInfo card) ;

     void changePassword(String email, String password) ;

     void changeAddress(String email, String address) ;

     void deleteInfo(String email);

     void deleteAccount(String email);
}
