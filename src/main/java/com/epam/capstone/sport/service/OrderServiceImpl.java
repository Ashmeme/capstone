package com.epam.capstone.sport.service;

import com.epam.capstone.sport.models.*;
import com.epam.capstone.sport.repository.*;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderStatusRepository orderStatusRepository;

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private ProductEntryRepository productEntryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    public String createOrder(String email) {
        User user = userRepository.findByEmail(email);

        if (null == user.getCard()) {
            return "redirect:/account";
        }
        if (null == user.getAddress()) {
            return "redirect:/account";
        }

        ShoppingCart cart = shoppingCartRepository.findByUser(user);
        List<ProductEntry> products = productEntryRepository.findAllByCart(cart);
        if (products.isEmpty()){
            return "redirect:/products";
        }

        OrderDetails order = new OrderDetails();
        order.setShoppingCart(cart);
        order.setUser(user);
        order.setCardInfo(user.getCard());
        order.setStatus(orderStatusRepository.findByName("Processing Order"));
        order.setShippingAddress(user.getAddress());
        order.setCreationDate(LocalDateTime.now());
        order.setModifiedDate(LocalDateTime.now());
        double totalPrice = 0.0;
        for (ProductEntry product :
                products) {
            int stock = product.getProduct().getStock();
            int quantity = product.getQuantity();
            if (stock < quantity) {
                return "redirect:/cart";
            }
            else {
                product.getProduct().setStock(stock - quantity);
            }

            totalPrice = totalPrice + quantity * product.getProduct().getPrice();
        }
        order.setTotalPrice(totalPrice);
        orderRepository.save(order);

        for (ProductEntry product :
                products) {
            productRepository.save(product.getProduct());
        }
        cart.setUser(null);
        cart.setOrderDetails(order);
        shoppingCartRepository.save(cart);
        ShoppingCart newCart = new ShoppingCart();
        newCart.setUser(user);
        shoppingCartRepository.save(newCart);
        return "redirect:/orders";
    }

    public List<OrderDetails> getAllOrders() {
        return orderRepository.findAll();
    }

    public List<OrderDetails> getAllOrdersByUser(Principal principal) {
        User user = userRepository.findByEmail(principal.getName());
        return orderRepository.findAllByUser(user);
    }

    public OrderDetails getOrderById(Long id) {
        return orderRepository.findById(id).orElseThrow();
    }

    public List<OrderStatus> getAllStatusesExceptCurrent(Long id) {
        OrderDetails order = getOrderById(id);
        List<OrderStatus> statuses = orderStatusRepository.findAll();
        statuses.remove(order.getStatus());
        return statuses;
    }

    public List<ProductEntry> getAllProductEntriesById(Long id) {
        OrderDetails order = getOrderById(id);
        return productEntryRepository.findAllByCart(order.getShoppingCart());
    }

    public OrderDetails updateOrderStatus(Long id, Long statusId) {
        OrderDetails order = orderRepository.findById(id).orElseThrow();
        order.setStatus(orderStatusRepository.findById(statusId).orElseThrow());
        order.setModifiedDate(LocalDateTime.now());
        return orderRepository.save(order);
    }

    public void deleteAllByUser(User user) {
        List<OrderDetails> orders = orderRepository.findAllByUser(user);

        for (OrderDetails order : orders) {
            ShoppingCart cart = order.getShoppingCart();
            if (cart != null) {
                cart.setOrderDetails(null);
                shoppingCartRepository.save(cart);
            }
            orderRepository.delete(order);
        }
    }
}
