package com.epam.capstone.sport.models;

import jakarta.persistence.*;

@Entity
@Table(name = "authGroup")
public class AuthGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String email;

    @Column
    private String authGroup;

    public AuthGroup(){};
    public AuthGroup(String email, String authGroup){

        this.email = email;
        this.authGroup = authGroup;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthGroup() {
        return authGroup;
    }

    public void setAuthGroup(String authGroup) {
        this.authGroup = authGroup;
    }
}
