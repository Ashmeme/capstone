package com.epam.capstone.sport;

import com.epam.capstone.sport.models.Category;
import com.epam.capstone.sport.models.Product;
import com.epam.capstone.sport.service.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;


import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerUnitTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductServiceImpl productService;

    @Test
    public void endpointProductById() throws Exception {
        Category category = new Category("Clothes");
        category.setId(1L);

        Product product = new Product();
        product.setId(1L);
        product.setName("Sample Product");
        product.setDescription("Sample Description");
        product.setBrand("Sample Brand");
        product.setPrice(100.0);
        product.setCategory(category);
        product.setStock(10);

        Mockito.when(productService.getProductById(1L)).thenReturn(product);

        mockMvc.perform(get("/products/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("product"))
                .andExpect(model().attributeExists("product"))
                .andExpect(model().attribute("product", hasProperty("id", is(1L))))
                .andExpect(model().attribute("product", hasProperty("name", is("Sample Product"))))
                .andExpect(model().attribute("product", hasProperty("description", is("Sample Description"))))
                .andExpect(model().attribute("product", hasProperty("brand", is("Sample Brand"))))
                .andExpect(model().attribute("product", hasProperty("price", is(100.0))))
                .andExpect(model().attribute("product", hasProperty("category", hasProperty("name", is("Clothes")))))
                .andExpect(model().attribute("product", hasProperty("stock", is(10))));
    }


}