package com.epam.capstone.sport.service;

import com.epam.capstone.sport.models.Category;
import com.epam.capstone.sport.models.Product;
import com.epam.capstone.sport.repository.CategoryRepository;
import com.epam.capstone.sport.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProductServiceImplTest {

    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllProducts() {
        Product product1 = new Product();
        Product product2 = new Product();
        List<Product> products = Arrays.asList(product1, product2);
        when(productRepository.findAll()).thenReturn(products);

        List<Product> result = productService.getAllProducts();

        verify(productRepository, times(1)).findAll();
        assertEquals(2, result.size());
    }

    @Test
    void getProductById() {
        Product product = new Product();
        when(productRepository.findById(1L)).thenReturn(Optional.of(product));

        Product result = productService.getProductById(1L);

        verify(productRepository, times(1)).findById(1L);
        assertNotNull(result);
    }

    @Test
    void getAllCategories() {
        Category category1 = new Category();
        Category category2 = new Category();
        List<Category> categories = Arrays.asList(category1, category2);
        when(categoryRepository.findAll()).thenReturn(categories);

        List<Category> result = productService.getAllCategories();

        verify(categoryRepository, times(1)).findAll();
        assertEquals(2, result.size());
    }

    @Test
    void createProduct_withProductAndCategoryId() {
        Product product = new Product();
        Category category = new Category();
        when(categoryRepository.findById(1L)).thenReturn(Optional.of(category));
        when(productRepository.save(product)).thenReturn(product);

        Product result = productService.createProduct(product, 1L);

        verify(categoryRepository, times(1)).findById(1L);
        verify(productRepository, times(1)).save(product);
        assertNotNull(result);
        assertEquals(category, product.getCategory());
    }

    @Test
    void createProduct_withDetails() {
        String name = "Product Name";
        String description = "Product Description";
        String brand = "Brand";
        String categoryName = "Category";
        double price = 100.0;
        int stock = 10;
        Category category = new Category();
        when(categoryRepository.findByName(categoryName)).thenReturn(category);
        ArgumentCaptor<Product> productCaptor = ArgumentCaptor.forClass(Product.class);
        when(productRepository.save(productCaptor.capture())).thenReturn(new Product());

        Product result = productService.createProduct(name, description, brand, categoryName, price, stock);

        verify(categoryRepository, times(1)).findByName(categoryName);
        verify(productRepository, times(1)).save(any(Product.class));
        assertNotNull(result);
        Product capturedProduct = productCaptor.getValue();
        assertEquals(name, capturedProduct.getName());
        assertEquals(description, capturedProduct.getDescription());
        assertEquals(brand, capturedProduct.getBrand());
        assertEquals(category, capturedProduct.getCategory());
        assertEquals(price, capturedProduct.getPrice());
        assertEquals(stock, capturedProduct.getStock());
    }

    @Test
    void updateProduct() {
        Long productId = 1L;
        Long categoryId = 2L;
        Product product = new Product();
        Category category = new Category();
        when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(category));
        when(productRepository.save(product)).thenReturn(product);

        Product result = productService.updateProduct(productId, product, categoryId);

        verify(categoryRepository, times(1)).findById(categoryId);
        verify(productRepository, times(1)).save(product);
        assertNotNull(result);
        assertEquals(productId, product.getId());
        assertEquals(category, product.getCategory());
    }

}
