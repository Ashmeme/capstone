package com.epam.capstone.sport.service;

import com.epam.capstone.sport.models.User;
import com.epam.capstone.sport.repository.ShoppingCartRepository;
import com.epam.capstone.sport.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserIntegrationTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;


    @BeforeEach
    void setUp() {
        shoppingCartRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void testRegisterAndRetrieveUser() {
        String email = "testuser@example.com";
        String password = "password123";
        userService.registerUser(email, password);

        User repoUser = userRepository.findByEmail(email);
        User serviceUser = userService.findByEmail(email);

        assertNotNull(serviceUser, "The user should be found");
        assertEquals(repoUser.getEmail(), serviceUser.getEmail(), "The email should match");
        assertEquals(repoUser.getPassword(), serviceUser.getPassword(), "The password should match");
    }
}
