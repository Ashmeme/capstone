package com.epam.capstone.sport.service;

import com.epam.capstone.sport.models.AuthGroup;
import com.epam.capstone.sport.models.CardInfo;
import com.epam.capstone.sport.models.ShoppingCart;
import com.epam.capstone.sport.models.User;
import com.epam.capstone.sport.repository.AuthGroupRepository;
import com.epam.capstone.sport.repository.CardInfoRepository;
import com.epam.capstone.sport.repository.ShoppingCartRepository;
import com.epam.capstone.sport.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private CardInfoRepository cardInfoRepository;
    @Mock
    private ShoppingCartRepository shoppingCartRepository;
    @Mock
    private AuthGroupRepository authGroupRepository;
    @Mock
    private BCryptPasswordEncoder encryptor;

    @InjectMocks
    private UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void registerUser() {
        String email = "test@example.com";
        String password = "password";
        when(encryptor.encode(password)).thenReturn("encodedPassword");

        userService.registerUser(email, password);

        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository, times(1)).save(userCaptor.capture());
        assertEquals(email, userCaptor.getValue().getEmail());
        assertEquals("encodedPassword", userCaptor.getValue().getPassword());

        ArgumentCaptor<ShoppingCart> cartCaptor = ArgumentCaptor.forClass(ShoppingCart.class);
        verify(shoppingCartRepository, times(1)).save(cartCaptor.capture());
        assertEquals(email, cartCaptor.getValue().getUser().getEmail());

        ArgumentCaptor<AuthGroup> authGroupCaptor = ArgumentCaptor.forClass(AuthGroup.class);
        verify(authGroupRepository, times(1)).save(authGroupCaptor.capture());
        assertEquals(email, authGroupCaptor.getValue().getEmail());
        assertEquals("USER", authGroupCaptor.getValue().getAuthGroup());
    }

    @Test
    void saveCardInfo() {
        String email = "test@example.com";
        CardInfo cardInfo = new CardInfo();
        User user = new User(email, "password");
        when(userRepository.findByEmail(email)).thenReturn(user);

        userService.saveCardInfo(email, cardInfo);

        verify(cardInfoRepository, times(1)).save(cardInfo);
        verify(userRepository, times(1)).save(user);
        assertEquals(cardInfo, user.getCard());
    }

    @Test
    void changePassword() {
        String email = "test@example.com";
        String newPassword = "newPassword";
        User user = new User(email, "oldPassword");
        when(userRepository.findByEmail(email)).thenReturn(user);
        when(encryptor.encode(newPassword)).thenReturn("encodedNewPassword");

        userService.changePassword(email, newPassword);

        verify(userRepository, times(1)).save(user);
        assertEquals("encodedNewPassword", user.getPassword());
    }

    @Test
    void changeAddress() {
        String email = "test@example.com";
        String newAddress = "newAddress";
        User user = new User(email, "password");
        when(userRepository.findByEmail(email)).thenReturn(user);

        userService.changeAddress(email, newAddress);

        verify(userRepository, times(1)).save(user);
        assertEquals(newAddress, user.getAddress());
    }

    @Test
    void deleteInfo() {
        String email = "test@example.com";
        User user = new User(email, "password");
        CardInfo cardInfo = new CardInfo();
        user.setCard(cardInfo);
        user.setAddress("address");
        when(userRepository.findByEmail(email)).thenReturn(user);

        userService.deleteInfo(email);

        verify(userRepository, times(1)).save(user);
        assertNull(user.getCard());
        assertNull(user.getAddress());
    }


}
