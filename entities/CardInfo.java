package com.epam.capstone.sport.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "cardInfo")
public class CardInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    // annotations for security

    @Column(name = "cardNumber")
    private String cardNumber;

    @Column(name = "expDate")
    private String expDate;

    @Column(name = "cardHolderName")
    private String CardHolderName;

    @Column(name = "cvv2")
    private String cvv2;


}
