package com.epam.capstone.sport.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(name = "email")
    private String email;

    @Column(name = "shippingAddress")
    private String shippingAddress;

    @NotNull
    @Column(name = "password")
    private String password;

    @OneToOne
    @JoinColumn(name = "cardInfo")
    private CardInfo cardInfo;
    public Long getId() {
        return id;
    }
}
