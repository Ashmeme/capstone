package com.epam.capstone.sport.entities;

import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "productEntry")
public class ProductEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shoppingCart")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ShoppingCart shoppingCart;



    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
