package com.epam.capstone.sport.entities;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name = "Order")
public class Order {
    @Id
    private Long id;

    @OneToOne
    @MapsId
    @JoinColumn(name = "User")
    private User user;

    @OneToOne
    @MapsId
    @JoinColumn(name = "shoppingCart")
    private ShoppingCart shoppingCart;

    @OneToOne
    @MapsId
    @JoinColumn(name = "cardInfo")
    private CardInfo cardInfo;
    
    @Column(name = "totalPrice")
    private double totalPrice;

    @Column(name = "creationDate")
    private Date creationDate;

    @Column(name = "modifiedDate")
    private Date modifiedDate;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
